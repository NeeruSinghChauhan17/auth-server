DROP SCHEMA IF EXISTS `auth` ;

CREATE DATABASE auth DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

use auth;

CREATE TABLE IF NOT EXISTS auth.project (
    pid INT AUTO_INCREMENT,
    pname VARCHAR(255) NOT NULL UNIQUE,
    reg_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    web_token_expire_time BIGINT,
    mobile_token_expire_time BIGINT,
    PRIMARY KEY (pid)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;
CREATE UNIQUE INDEX pname_project_indx ON project (pname);

CREATE TABLE IF NOT EXISTS auth.client (
    cid INT AUTO_INCREMENT,
    pid INT,
    secret VARCHAR(10) NOT NULL UNIQUE,
    cname VARCHAR(255) NOT NULL UNIQUE,
    permission TINYINT(1) DEFAULT 0 COMMENT 'Permission : 0(READ), 1(WRITE)',
    reg_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (cid),
    FOREIGN KEY (pid) REFERENCES project(pid)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS auth.token (
    pid INT,
    user_id VARCHAR(255),
    user_agent VARBINARY(16),
    token TEXT,
    PRIMARY KEY (pid,user_id,user_agent),
    FOREIGN KEY (pid) REFERENCES project(pid)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS auth.blocked_user (
    pid INT,
    user_id VARCHAR(255),
    PRIMARY KEY (pid,user_id),
    FOREIGN KEY (pid) REFERENCES project(pid)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS auth.oauth_client_secret (
    secret_id INT,
    secret VARCHAR(10) NOT NULL,
    role TINYINT(1) DEFAULT 0 COMMENT 'Role : 0(User), 1(Admin)',
    PRIMARY KEY (secret_id)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;

CREATE INDEX secret_oauth_client_secret_indx ON oauth_client_secret (secret);

CREATE TABLE IF NOT EXISTS auth.admin (
    secret_id VARCHAR(255) NOT NULL,
    secret VARCHAR(15) NOT NULL,
    PRIMARY KEY (secret_id)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;


 CREATE TABLE IF NOT EXISTS auth.signature_keys(
 	private_key VARCHAR(255),
 	private_value VARCHAR(255),
 	PRIMARY KEY(private_key , private_value)
 ) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS auth.signature_credential(
   pid INT,	
   admin_id VARCHAR(255),
   token TEXT,
   private_key VARCHAR(255),
   private_value VARCHAR(255),
   user_agent VARBINARY(16),
   PRIMARY KEY (pid , admin_id ,user_agent ),
   FOREIGN KEY ( private_key ) REFERENCES signature_keys( private_key ),
   FOREIGN KEY (pid) REFERENCES project(pid)
) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci ;



#insert into oauth_client_secret(secret_id,secret,role) values(123,'test',1);
