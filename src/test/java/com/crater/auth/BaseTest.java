/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

/**
 * @author Navrattan Yadav
 *
 */
public class BaseTest {

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
}
