package com.crater.auth;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.crater.auth.application.AuthApplication;
import com.google.common.base.Throwables;

/**
 * User: pwyrwins
 * Date: 2/16/13
 * Time: 2:44 PM
 */
public class TestContext extends TestWatcher {

    private static final Logger LOG = LoggerFactory.getLogger(TestContext.class);

    private static Boolean IN_SUITE = false;
    private AuthApplication application;
    private final Boolean forSuite;

    public TestContext(Boolean forSuite) {
        this.forSuite = forSuite;
        if (this.forSuite) {
            IN_SUITE = true;
        }
    }

    @Override
    protected void starting(Description description) {
        LOG.info("{} starting for suite: {}.", this.getClass().getSimpleName(), forSuite);
        if (forSuite == IN_SUITE) {
            LOG.info("{} {} calling START dropWizard Server.", this.getClass().getSimpleName());
            startDropWizardServer();
        }
    }

    @Override
    protected void finished(Description description) {
        LOG.info("{} finishing for suite: {}.", this.getClass().getSimpleName(), forSuite);
        if (forSuite == IN_SUITE) {
            LOG.info("{} calling STOP dropWizard Server.", this.getClass().getSimpleName());
            stopDropWizardServer();
        }
    }

    private void startDropWizardServer() {
        application = new AuthApplication();
        try {
            application.startEmbeddedServer(this.getClass().getClassLoader()
                    .getResource("auth-service.yaml").getPath());
        } catch (Exception e) {
            LOG.error("Exception starting server {}", e.getMessage());
        }

        if(!application.isEmbeddedServerRunning()) {
            throw new RuntimeException("Server did not start correctly");
        }
    }

    private void stopDropWizardServer() {
        if(application.isEmbeddedServerRunning()) {
            try {
                application.stopEmbeddedServer();
            } catch (Exception e) {
                throw Throwables.propagate(e);
            }
        }
    }

}
