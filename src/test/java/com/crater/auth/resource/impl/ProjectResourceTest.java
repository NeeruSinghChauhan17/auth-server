/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource.impl;

import static org.mockito.Mockito.*;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.crater.auth.BaseTest;
import com.crater.auth.exception.BadRequestException;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;
import com.crater.auth.service.ProjectService;

/**
 * @author Navrattan Yadav
 *
 */
public class ProjectResourceTest extends BaseTest {

	
	private ProjectService projectService;
	
	private ProjectResourceImpl projectResource;
	
	private Project project;
	
	@Before
	public void setUp() {
		initMocks();
		projectService = mock(ProjectService.class);
		project = mock(Project.class);
		projectResource = new ProjectResourceImpl(projectService);
		
	}
	
	/*@Test
	public void testCreate() throws ConflitException {
		doReturn(project).when(projectService).add(project);
		final Project projectAfterCreate = projectResource.create(project);
		Assert.assertEquals(project, projectAfterCreate);
	}
	
	@Test(expected=ConflitException.class)
	public void testCreateConflit() throws ConflitException {
		doThrow(ConflitException.class).when(projectService).add(project);
		projectResource.create(project);
	}
	
	@Test
	public void testDelete() throws ForbiddenException {
		doNothing().when(projectService).delete(1);
		final Response response = projectResource.delete(1);
		Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
		
	}
*/
}
