/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crater.auth.annotation.Creator;
import com.crater.auth.annotation.UserRole;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.exception.UnAuthorizedException;
import com.crater.auth.model.AccessToken;

/**
 * @author Navrattan Yadav
 *
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/v1/projects/{projectId}/clients/{clientId}/tokens")
public interface TokenResource {
	
	/**
	 * Create A new Token
	 */
	@PUT
	@UserRole
	@Creator
	public abstract AccessToken create(@PathParam("projectId") int projectId, 
			@PathParam("clientId") int clientId,
			@Valid @NotNull AccessToken token) throws ForbiddenException;
	
	/**
	 * Authorize Token
	 */
	@POST
	@UserRole
	@Path("/authorize")
	public abstract Response authorize(@PathParam("projectId") int projectId,
			@PathParam("clientId") int clientId, @Valid @NotNull AccessToken token)
					throws UnAuthorizedException;
	
	/**
	 * Delete a Token
	 */
	@DELETE
	@UserRole
	public abstract Response delete(@PathParam("projectId") int projectId,
			@PathParam("clientId") int clientId, @Valid @NotNull AccessToken token)
					throws ForbiddenException;

}
