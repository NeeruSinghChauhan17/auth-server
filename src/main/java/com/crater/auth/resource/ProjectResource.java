/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crater.auth.annotation.AdminRole;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;

/**
 * @author Navrattan Yadav
 * 
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/v1/projects")
public interface ProjectResource {

	/**
	 * Add New Project
	 */
	@PUT
	@AdminRole
	public abstract Project create(@Valid @NotNull Project project) throws ConflitException;
	
	/**
	 * Get Project
	 */
	@GET
	@AdminRole
	@Path("/{projectId}")
	public abstract Project get(@PathParam("projectId") int projectId) throws ForbiddenException;
	
	/**
	 * Get Project
	 */
	@GET
	@AdminRole
	@Path("/list")
	public abstract List<Project> list(@QueryParam("count") int count);

	/**
	 * Delete Project
	 */
	@DELETE
	@AdminRole
	@Path("/{projectId}")
	public abstract Response delete(@PathParam("projectId") int projectId) throws ForbiddenException;
	
	/**
	 * Update Token Expire Time
	 */
	@POST
	@AdminRole
	@Path("/{projectId}")
	public abstract Response updateTokenExpireTime(@PathParam("projectId") int projectId,
			@NotNull TokenExpireTime tokenExpireTime) throws ForbiddenException;
}
