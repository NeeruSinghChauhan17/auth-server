/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.annotation.AdminRole;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;
import com.crater.auth.resource.ProjectResource;
import com.crater.auth.service.ProjectService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class ProjectResourceImpl implements ProjectResource {

	private final ProjectService projectService;
	
	@Autowired
	public ProjectResourceImpl(ProjectService projectService) {
		this.projectService = projectService;
	}

	@Override
	@AdminRole
	public Project create(Project project) throws ConflitException {
		return projectService.add(project);
	}

	@Override
	@AdminRole
	public Response delete(int projectId) throws ForbiddenException {
		projectService.delete(projectId);
		return Response.noContent().build();
	}

	@Override
	@AdminRole
	public Response updateTokenExpireTime(int projectId, TokenExpireTime tokenExpireTime) throws ForbiddenException {
		projectService.updateTokenExpireTime(projectId, tokenExpireTime);
		return Response.noContent().build();
	}

	@Override
	@AdminRole
	public Project get(int projectId) throws ForbiddenException {
		return projectService.get(projectId);
	}

	@Override
	@AdminRole
	public List<Project> list(int count) {
		return projectService.list(count);
	}

}
