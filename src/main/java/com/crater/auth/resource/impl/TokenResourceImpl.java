/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.annotation.Creator;
import com.crater.auth.annotation.UserRole;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.exception.UnAuthorizedException;
import com.crater.auth.model.AccessToken;
import com.crater.auth.resource.TokenResource;
import com.crater.auth.service.TokenService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class TokenResourceImpl implements TokenResource {

	private final TokenService tokenService;
	
	@Autowired
	public TokenResourceImpl(TokenService tokenService) {
		this.tokenService = tokenService;
	}

	@Override
	@UserRole
	@Creator
	public AccessToken create(int projectId, int clientId, AccessToken token) throws ForbiddenException {
		return tokenService.create(projectId, clientId, token);
	}

	@Override
	@UserRole
	public Response authorize(int projectId, int clientId, AccessToken token) 
			throws UnAuthorizedException {
		if(tokenService.authorize(projectId, token)) {
			return Response.noContent().build();
		}
		return Response.status(Response.Status.UNAUTHORIZED).build();
	}

	@Override
	@UserRole
	public Response delete(int projectId, int clientId, AccessToken token) throws ForbiddenException {
		tokenService.delete(projectId, token);
		return Response.noContent().build();
	}
}
