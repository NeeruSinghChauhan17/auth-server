/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.annotation.AdminRole;
import com.crater.auth.annotation.Log;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Client;
import com.crater.auth.model.ClientPermission;
import com.crater.auth.resource.ClientResource;
import com.crater.auth.service.ClientService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class ClientResourceImpl implements ClientResource {

	private final ClientService clientService;
	
	@Autowired
	public ClientResourceImpl (ClientService clientService) {
		this.clientService = clientService;
	}
	
	@Log
	@Override
	@AdminRole
	public Client create(int projectId, Client client) throws ConflitException {
		return clientService.create(projectId, client);
	}

	@Override
	@AdminRole
	public Client get(int projectId, int clientId) throws ForbiddenException {
		return clientService.getClientById(clientId);
	}

	@Override
	@AdminRole
	public List<Client> getAllClientForProjectId(int projectId) {
		return clientService.getAllClientForProjectId(projectId);
	}

	@Override
	@AdminRole
	public Response delete( int projectId, int clientId) throws ForbiddenException {
		clientService.delete(clientId);
		return Response.noContent().build();
	}

	@Override
	@AdminRole
	public Response updatePermission(int projectId, int clientId,
			ClientPermission permission) throws ForbiddenException {
		clientService.updatePermission(clientId, permission.getPermission());
		return Response.noContent().build();
	}

}
