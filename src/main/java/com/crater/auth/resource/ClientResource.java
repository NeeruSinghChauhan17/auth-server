/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.resource;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crater.auth.annotation.AdminRole;
import com.crater.auth.annotation.Log;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Client;
import com.crater.auth.model.ClientPermission;
import com.crater.auth.model.TokenPermission;

/**
 * @author Navrattan Yadav
 *
 */

@Path("/v1/projects/{projectId}/clients")
public interface ClientResource {
	/**
	 * Add New Client in A project
	 */
	@Log
	@PUT
	@AdminRole
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public abstract Client create(@PathParam("projectId") int projectId, 
			@Valid @NotNull Client client) throws ConflitException;
	
	/**
	 * Get Client By Client Id
	 */
	@GET
	@AdminRole
	@Path("/{clientId}")
	@Produces(MediaType.APPLICATION_JSON)
	public abstract Client get(@PathParam("projectId") int projectId,
			@PathParam("clientId") int clientId) throws ForbiddenException;
	
	/**
	 * Get All Client for A project
	 */
	@GET
	@AdminRole
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public abstract List<Client> getAllClientForProjectId(@PathParam("projectId") int projectId);

	/**
	 * Delete a Client
	 */
	@DELETE
	@AdminRole
	@Path("/{clientId}")
	public abstract Response delete(@PathParam("projectId") int projectId,
			@PathParam("clientId") int clientId) throws ForbiddenException;
	
	/**
	 * Update Client Permission for Tokens  : {@link TokenPermission}
	 */
	@POST
	@AdminRole
	@Path("/{clientId}")
	public abstract Response updatePermission(@PathParam("projectId") int projectId,
			@PathParam("clientId") int clientId, @NotNull ClientPermission permission)
					throws ForbiddenException;

}
