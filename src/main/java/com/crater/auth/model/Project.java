/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.model;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Navrattan Yadav
 *
 */
@JsonInclude(Include.NON_DEFAULT)
public class Project {

	private final int projectId;
	
	@NotBlank(message = "Project name can't be null")
	private final String name;
	
	private final TokenExpireTime tokenExpireTime;
	

	@SuppressWarnings("unused")
	private Project() {
		this(0,null,null);
	}
	
	public Project(int projectId, String name, TokenExpireTime tokenExpireTime) {
		this.projectId = projectId;
		this.name = name;
		this.tokenExpireTime = tokenExpireTime;
	}

	@JsonProperty("project_id")
	public int getProjectId() {
		return projectId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("token_expire_time")
	public TokenExpireTime getTokenExpireTime() {
		return tokenExpireTime;
	}
	
	public static final Project createWithTokenExpireTime(final Project project, 
			final TokenExpireTime tokenExpireTime) {
		return new Project(project.getProjectId(), project.getName(), tokenExpireTime);
	}
	
	public static final Project createWithProjectId(final Project project, 
			final int projectId) {
		return new Project(projectId, project.getName(), project.getTokenExpireTime());
	}
	
}