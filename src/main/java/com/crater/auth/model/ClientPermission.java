/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Navrattan Yadav
 *
 */
public class ClientPermission {

	private final TokenPermission permission;

	@SuppressWarnings("unused")
	private ClientPermission() {
		this(null);
	}

	public ClientPermission(TokenPermission permission) {
		this.permission = permission;
	}

	@JsonProperty("permission")
	public TokenPermission getPermission() {
		return permission;
	}
	
	
}
