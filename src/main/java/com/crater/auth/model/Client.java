/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.model;



import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Navrattan Yadav
 *
 */
@JsonInclude(Include.NON_DEFAULT)
public class Client {

	private final int clientId;
	
	@NotBlank(message = "Client name can't be null")
	private final String name;
	
	private final String secret;
	
	private final TokenPermission permission;
	
	@SuppressWarnings("unused")
	private Client() {
		this(0,null,null,null);
	}

	public Client(int clientId, String name, String secret, TokenPermission permission) {
		this.clientId = clientId;
		this.name = name;
		this.secret = secret;
		this.permission = permission;
	}

	@JsonProperty("client_id")
	public int getClientId() {
		return clientId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("secret")
	public String getSecret() {
		return secret;
	}

	@JsonProperty("permission")
	public TokenPermission getPermission() {
		return permission;
	}
	
	public static final Client createClientWithSecret( final Client client, final String secret) {
		return new Client(client.getClientId(), client.getName(), secret, client.getPermission());
	}
	
	public static final Client createClientWithClientId( final Client client, final int clientId) {
		return new Client(clientId, client.getName(), client.getSecret(), client.getPermission());
	}
}
