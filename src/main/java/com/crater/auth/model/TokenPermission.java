/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.model;


/**
 * @author Navrattan Yadav
 *
 */
public enum TokenPermission {

	/**
	 * Allow to Authorize a token
	 */
	AUTHORIZE_ONLY,

	/**
	 * Allow to create and authorize
	 */
	CREATE_AND_AUTHORIZE;
	
	public static int convertPermissionToIntValue(TokenPermission permission) {
		switch (permission) {
		case AUTHORIZE_ONLY:
			return 0;
		case CREATE_AND_AUTHORIZE:
			return 1;
		default:
			return 0;
		}
	}

	public static TokenPermission convertIntValueToTokenPermission(int permission) {
		switch (permission) {
		case 0:
			return AUTHORIZE_ONLY;
		case 1:
			return CREATE_AND_AUTHORIZE;
		default:
			return AUTHORIZE_ONLY;
		}
	}
}
