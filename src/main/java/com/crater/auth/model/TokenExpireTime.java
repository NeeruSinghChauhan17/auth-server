/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Navrattan Yadav
 *
 */
@JsonInclude(Include.NON_DEFAULT)
public class TokenExpireTime {

	private final long webTokenExpireTime;
	private final long mobileTokenExpireTime;
	
	@SuppressWarnings("unused")
	private TokenExpireTime() {
		this(0,0);
	}
	
	public TokenExpireTime(long webTokenExpireTime, long mobileTokenExpireTime) {
		this.webTokenExpireTime = webTokenExpireTime;
		this.mobileTokenExpireTime = mobileTokenExpireTime;
	}

	@JsonProperty("web_token_expire_time")
	public long getWebTokenExpireTime() {
		return webTokenExpireTime;
	}

	@JsonProperty("mobile_token_expire_time")
	public long getMobileTokenExpireTime() {
		return mobileTokenExpireTime;
	}
	
}
