/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.model;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Navrattan Yadav
 *
 */
public class AccessToken {

	private final String token;
	
	@NotBlank(message = "User ID can't be blank")
	private final String userId;
	
	@NotBlank(message = "UserAgent can't be blank")
	private String userAgent;
	
	@SuppressWarnings("unused")
	private AccessToken() {
		this(null,null,null);
	}

	public AccessToken(String token, String userId, String userAgent) {
		this.token = token;
		this.userId = userId;
		this.userAgent = userAgent;
	}

	@JsonProperty("token")
	public String getToken() {
		return token;
	}

	@JsonProperty("user_id")
	public String getUserId() {
		return userId;
	}

	@JsonProperty("user_agent")
	public String getUserAgent() {
		return userAgent;
	}
	
	public static final AccessToken createAccessTokenWithToken(final AccessToken accessToken,
			final String token) {
		return new AccessToken(token, accessToken.getUserId(), accessToken.getUserAgent());
	}
	
}
