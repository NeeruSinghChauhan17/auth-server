/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao;

import java.util.List;

import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;

/**
 * @author Navrattan Yadav
 *
 */
public interface ProjectDao {

	public abstract Project create(Project project) throws ConflitException;
	
	public abstract void delete(int projectId) throws ForbiddenException;
	
	public abstract List<Project> list(int skip, int limit);
	
	public abstract Project getProjectByProjectId(int projectId) throws ForbiddenException;
	
	public abstract void updateTokenExpireTime(int projectId, TokenExpireTime time)
			throws ForbiddenException;
	
	public abstract TokenExpireTime getTokenExpireTime(int projectId)
			throws ForbiddenException;
}
