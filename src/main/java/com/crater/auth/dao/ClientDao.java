/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao;

import java.util.List;

import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Client;
import com.crater.auth.model.TokenPermission;

/**
 * @author Navrattan Yadav
 *
 */
public interface ClientDao {

	public abstract Client create(int projectId, Client client)
			throws ConflitException;
	
	public abstract void delete(int clientId)
			throws ForbiddenException;
	
	public abstract void updatePermission(int clientId,
			TokenPermission permission) throws ForbiddenException;
	
	public abstract Client getClientById(int clientId) throws ForbiddenException;
	
	public abstract List<Client> getAllClientForProjectId(int projectId);
}
