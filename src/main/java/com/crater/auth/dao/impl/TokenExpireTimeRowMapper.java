/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.crater.auth.model.TokenExpireTime;

/**
 * @author Navrattan Yadav
 *
 */
public class TokenExpireTimeRowMapper extends ProjectRowMapper<TokenExpireTime>{

	@Override
	public TokenExpireTime mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TokenExpireTime(rs.getLong("web_token_expire_time"),
				rs.getLong("mobile_token_expire_time"));
	}
}
