/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import com.crater.auth.dao.BaseDao;

/**
 * @author Navrattan Yadav
 * 
 */
public class BaseDaoImpl implements BaseDao {

	private final NamedParameterJdbcOperations jdbcTemplate;

	private final Map<String, String> queryMap;

	protected Logger logger;

	public BaseDaoImpl(final Map<String, String> queryMap,
			final NamedParameterJdbcOperations jdbcTemplate) {
		this.queryMap = queryMap;
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public NamedParameterJdbcOperations getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	public String getQueryById(final String id) {
		return queryMap.get(id);
	}

}
