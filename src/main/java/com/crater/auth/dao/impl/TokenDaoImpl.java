/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.crater.auth.dao.BaseDao;
import com.crater.auth.dao.TokenDao;
import com.crater.auth.model.AccessToken;

/**
 * @author Navrattan Yadav
 *
 */
@Repository
public class TokenDaoImpl implements TokenDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenDaoImpl.class);
	
	
	private final BaseDao baseDao;
	
	@Autowired
	public TokenDaoImpl(BaseDaoFactory baseDaoFactory) {
		baseDao = checkNotNull(baseDaoFactory.createBaseDao("token.xml"), "Base Dao can not be null");
	}

	@Override
	public void save(int projectId, AccessToken accessToken) {
		final String saveQuery = baseDao.getQueryById("saveToken");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
				addValue("pId", projectId ).
				addValue("userId", accessToken.getUserId()).
				addValue("userAgent", accessToken.getUserAgent()).
				addValue("token", accessToken.getToken());
		baseDao.getJdbcTemplate().update(saveQuery, sqlParameterSource);
	}

	@Override
	public void delete(int projectId, String userId, String userAgent) {
		final String deleteQuery = baseDao.getQueryById("deleteToken");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
				addValue("pId", projectId ).
				addValue("userId", userId).
				addValue("userAgent", userAgent);
		if( baseDao.getJdbcTemplate().update(deleteQuery, sqlParameterSource) == 0) {
			LOGGER.error("Delete Token : No token found for projectId = {} ,UserId = {} and UserAgent = {}",
					projectId, userId, userAgent);
		}
	}

	@Override
	public String get(int projectId, String userId, String userAgent) {
		try {
			final String query = baseDao.getQueryById("getToken");
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
					addValue("pId", projectId ).
					addValue("userId", userId).
					addValue("userAgent", userAgent);
			return baseDao.getJdbcTemplate().queryForObject(query, sqlParameterSource, String.class);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
}
