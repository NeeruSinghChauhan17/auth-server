/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;

/**
 * @author Navrattan Yadav
 *
 */
public class ProjectRowMapper<T> implements RowMapper<T> {
	
	@SuppressWarnings("unchecked")
	@Override
	public T mapRow(ResultSet rs, int rowNum) throws SQLException {
		return (T) new Project(rs.getInt("pid"), rs.getString("pname"),
				new TokenExpireTime(rs.getLong("web_token_expire_time"),
						rs.getLong("mobile_token_expire_time")));
	}

}
