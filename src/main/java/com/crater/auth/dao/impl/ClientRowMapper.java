/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.crater.auth.model.Client;
import com.crater.auth.model.TokenPermission;

/**
 * @author Navrattan Yadav
 *
 */
public class ClientRowMapper implements RowMapper<Client> {

	@Override
	public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Client(rs.getInt("cid"), rs.getString("cname"), rs.getString("secret"),
				TokenPermission.convertIntValueToTokenPermission(rs.getInt("permission")));
	}
}
