/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.crater.auth.dao.BaseDao;
import com.crater.auth.dao.ProjectDao;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;

/**
 * @author Navrattan Yadav
 *
 */
@Repository
public class ProjectDaoImpl implements ProjectDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectDaoImpl.class);
	
	private static final ProjectRowMapper<Project> PROJECT_ROW_MAPPER = new ProjectRowMapper<Project>();
	
	private static final TokenExpireTimeRowMapper TOKEN_EXPIRE_TIME_ROW_MAPPER = new TokenExpireTimeRowMapper();
	
	private static final String[] PROJECT_ID_COLUMN_NAME = { "pid" };
	
	private final BaseDao baseDao;
	
	@Autowired
	public ProjectDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("project.xml"), "Base Dao Cannot Be Null");

	}
	
	@Override
	public Project create(Project project) throws ConflitException {
		try {
			final String saveQuery = baseDao.getQueryById("saveProject");
			final KeyHolder keyHolder = new GeneratedKeyHolder();
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
					addValue("pName", project.getName()).
					addValue("webTokenExpireTime", project.getTokenExpireTime().getWebTokenExpireTime()).
					addValue("mobileTokenExpireTime", project.getTokenExpireTime().getMobileTokenExpireTime());
			baseDao.getJdbcTemplate().update(saveQuery, sqlParameterSource, keyHolder, PROJECT_ID_COLUMN_NAME);
			return Project.createWithProjectId(project, keyHolder.getKey().intValue());
		} catch (DataIntegrityViolationException e ) {
			
			LOGGER.error("error while add project : project with given name :" + project.getName() +
					" already exist", e.getMessage());
			
			throw new ConflitException.Builder().message( new StringBuilder().
					append("Project with name ").
					append( project.getName()).
					append(" is already exist").toString()).build();
		}
	}

	@Override
	public void delete(int projectId) throws ForbiddenException {
		final String deleteQuery = baseDao.getQueryById("deleteProject");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
				addValue("pId", projectId);

		if( baseDao.getJdbcTemplate().update(deleteQuery, sqlParameterSource) == 0 ) {
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("project with id ").
					append(projectId).
					append(" is not exist").toString()).build();
		}
	}

	@Override
	public void updateTokenExpireTime(int projectId, TokenExpireTime time)
			throws ForbiddenException {
		final String updateQuery = baseDao.getQueryById("updateTokenExpireTime");
		
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
				addValue("webTokenExpireTime", time.getWebTokenExpireTime()).
				addValue("mobileTokenExpireTime", time.getMobileTokenExpireTime()).
				addValue("pId", projectId);
		
		if( baseDao.getJdbcTemplate().update(updateQuery, sqlParameterSource) == 0) {
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("project with id ").
					append(projectId).
					append(" is not exist").toString()).build();
		}
	}

	@Override
	public TokenExpireTime getTokenExpireTime(int projectId)
			throws ForbiddenException {
		try {
			final String query = baseDao.getQueryById("getTokenExpireTime");
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
					addValue("pId", projectId);
			return baseDao.getJdbcTemplate().
					queryForObject(query, sqlParameterSource, TOKEN_EXPIRE_TIME_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("project with id ").
					append(projectId).
					append(" is not exist").toString()).build();
		}
	}


	@Override
	public Project getProjectByProjectId(int projectId) throws ForbiddenException {
		try {
			final String query = baseDao.getQueryById("getProject");
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
					addValue("pId", projectId);
			return baseDao.getJdbcTemplate().queryForObject(query,
					sqlParameterSource, PROJECT_ROW_MAPPER);
		} catch (EmptyResultDataAccessException e) {
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("project with id ").
					append(projectId).
					append(" is not exist").toString()).build();
		}
	}

	@Override
	public List<Project> list(int skip, int limit) {
		final String query = baseDao.getQueryById("getProjectList");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
				addValue("skip", skip).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, sqlParameterSource, PROJECT_ROW_MAPPER);
		
	}

}
