/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.crater.auth.dao.BaseDao;
import com.crater.auth.dao.ClientDao;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Client;
import com.crater.auth.model.TokenPermission;

/**
 * This Class is Repository For Client. It also Provide cache for client.
 * 
 * Cache : Spring Default Cache
 * 
 * @author Navrattan Yadav
 *
 */
@Repository
public class ClientDaoImpl implements ClientDao {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDaoImpl.class);
	
	private static final ClientRowMapper CLIENT_ROW_MAPPER = new ClientRowMapper(); 
	
	private static final String[] CLIENT_ID_COLUMN_NAME = { "cid" };
	
	private final BaseDao baseDao;
	
	@Autowired
	public ClientDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = checkNotNull(baseDaoFactory.createBaseDao("client.xml"), "Base Dao Cannot Be Null");

	}

	/**
	 * NOTE : We did not put client in cache here because we need clientId at
	 * method calling time which is not available and generate 
	 * inside the method after client saved in db.
	 */
	@Override
	public Client create(int projectId, Client client) throws ConflitException {
		try {
			final String saveQuery = baseDao.getQueryById("saveClient");
			final KeyHolder keyHolder = new GeneratedKeyHolder();
			final SqlParameterSource sqlParameterSource = new MapSqlParameterSource().
					addValue("pId", projectId).
					addValue("secret", client.getSecret()).
					addValue("cName", client.getName()).
					addValue("permission",
							TokenPermission.convertPermissionToIntValue(client.getPermission()));
			
			baseDao.getJdbcTemplate().update(saveQuery, sqlParameterSource, keyHolder,
					CLIENT_ID_COLUMN_NAME);
			
			return Client.createClientWithClientId(client, keyHolder.getKey().intValue());
		} catch (DataIntegrityViolationException e ) {
			LOGGER.error(" Error while add Client: Client with given name = " + client.getName() +
					" is already exist ", e.getMessage());
			
			throw new ConflitException.Builder().message( new StringBuilder().
					append("Client with name ").
					append(client.getName()).
					append(" is already exist").toString()).build();
		}
	}

	@CacheEvict( value="clients" ,key="#clientId")
	@Override
	public void delete(int clientId) throws ForbiddenException {
		final String deleteQuery = baseDao.getQueryById("deleteClient");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("cId", clientId);
		
		if ( baseDao.getJdbcTemplate().update(deleteQuery, source)  == 0) {
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("client with id ").
					append(clientId).
					append(" is not exist").toString()).build();
		}
	}

	@CacheEvict(value="clients" ,key="#clientId")
	@Override
	public void updatePermission(int clientId,
			TokenPermission permission) throws ForbiddenException {
		final String updateQuery = baseDao.getQueryById("updatePermission");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("permission", TokenPermission.convertPermissionToIntValue(permission)).
				addValue("cId", clientId);
		
		if (baseDao.getJdbcTemplate().update(updateQuery, source) == 0 ) {
			
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("client with id ").
					append(clientId).
					append(" is not exist").toString()).build();
		}
	}

	@Cacheable(value="clients" ,key="#clientId")
	@Override
	public Client getClientById(int clientId) throws ForbiddenException {
		try {
			//slowQuery(10000l);
			final String query = baseDao.getQueryById("getClientById");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("cId", clientId);
			return baseDao.getJdbcTemplate().queryForObject(query, source, CLIENT_ROW_MAPPER);
			
		} catch (EmptyResultDataAccessException e) {
			throw new ForbiddenException.Builder().message( new StringBuilder().
					append("client with id ").
					append(clientId).
					append(" is not exist").toString()).build();
		}
	}


	private void slowQuery(long seconds){
	    try {
                Thread.sleep(seconds);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
	}
	
	@Override
	public List<Client> getAllClientForProjectId(int projectId) {
		final String query = baseDao.getQueryById("getAllClientForProjectId");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("pId", projectId);
		return baseDao.getJdbcTemplate().query(query, source, CLIENT_ROW_MAPPER);
	}
}
