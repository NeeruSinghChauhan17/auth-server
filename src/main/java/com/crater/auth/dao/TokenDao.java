/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao;

import com.crater.auth.model.AccessToken;

/**
 * @author Navrattan Yadav
 *
 */
public interface TokenDao {

	public abstract void save(int projectId,AccessToken accessToken);
	public abstract void delete(int projectId, String userId, String userAgent);
	public abstract String get(int projectId, String userId, String userAgent);
}
