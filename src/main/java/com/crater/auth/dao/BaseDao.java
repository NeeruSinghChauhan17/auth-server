/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.dao;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

/**
 * @author Navrattan Yadav
 *
 */
public interface BaseDao {

	public NamedParameterJdbcOperations getJdbcTemplate();

	public String getQueryById(String id);
}
