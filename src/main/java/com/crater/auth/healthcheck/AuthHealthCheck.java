/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.healthcheck;

import org.springframework.stereotype.Component;

import com.codahale.metrics.health.HealthCheck;

/**
 * @author Navrattan Yadav
 *
 */
@Component
public class AuthHealthCheck extends HealthCheck {

	@Override
	protected Result check() throws Exception {
		
		return Result.healthy("Working..");
	}

}
