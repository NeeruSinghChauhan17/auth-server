/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.exception;


import org.springframework.http.HttpStatus;

/**
 * @author Navrattan Yadav
 *
 */
public class BadRequestException extends CustomException {

	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.BAD_REQUEST;
		private String message = "Bad Request";
		private Throwable cause;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public BadRequestException build() {
			return new BadRequestException(this);
		}
	}

	public BadRequestException( Builder builder) {
		super(builder.message, builder.status, builder.cause );
	}
}

