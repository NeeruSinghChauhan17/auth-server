/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.exception;

import org.springframework.http.HttpStatus;


/**
 * @author Navrattan Yadav
 *
 */
public class InternalServerError extends CustomException {
	
	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		private String message = "Internal Server Error";
		private Throwable cause;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public InternalServerError build() {
			return new InternalServerError(this);
		}
	}

	
	private InternalServerError( Builder builder) {
		super(builder.message, builder.status, builder.cause );
	}
}
