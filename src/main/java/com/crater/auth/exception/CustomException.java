/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.exception;

import org.springframework.http.HttpStatus;


/**
 * @author Navrattan Yadav
 *
 */
public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * status : HttpStatus Code in Exception
	 */
	private final HttpStatus status ;

	public CustomException(String message, HttpStatus status, Throwable cause) {
		super(message, cause);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}
}
