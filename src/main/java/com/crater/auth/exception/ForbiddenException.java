/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.exception;

import org.springframework.http.HttpStatus;


/**
 * @author Navrattan Yadav
 * 
 */
public class ForbiddenException extends CustomException {

	private static final long serialVersionUID = 1L;

	public static class Builder {
		
		private HttpStatus status = HttpStatus.FORBIDDEN;
		private String message = "Forbidden";
		private Throwable cause;
		
		public Builder() {
			
		}
		
		public Builder message( String message) {
			this.message = message;
			return this;
		}
		
		public Builder throwable( Throwable cause ) {
			this.cause = cause;
			return this;
		}
		
		public ForbiddenException build() {
			return new ForbiddenException(this);
		}
	}

	private ForbiddenException( Builder builder) {
		super(builder.message, builder.status, builder.cause );
	}
}
