/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.exception;

import org.springframework.http.HttpStatus;

/**
 * This Exception Used For Unauthorized Access and must be mapped to HTTP STATUS
 * 401
 * 
 * @author navrattan
 * 
 */
public class UnAuthorizedException extends CustomException {

	private static final long serialVersionUID = 1L;

	public static class Builder {

		private HttpStatus status = HttpStatus.UNAUTHORIZED;
		private String message = "UnAuthorized";
		private Throwable cause;

		public Builder() {

		}

		public Builder message(String message) {
			this.message = message;
			return this;
		}

		public Builder throwable(Throwable cause) {
			this.cause = cause;
			return this;
		}

		public UnAuthorizedException build() {
			return new UnAuthorizedException(this);
		}
	}

	private UnAuthorizedException(Builder builder) {
		super(builder.message, builder.status, builder.cause);
	}
}
