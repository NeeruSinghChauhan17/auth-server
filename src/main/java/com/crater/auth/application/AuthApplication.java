/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.application;

import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;
import jersey.repackaged.com.google.common.collect.Lists;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.crater.auth.filter.AuthorizationFilter;
import com.crater.auth.healthcheck.AuthHealthCheck;
import com.crater.auth.provider.CustomExceptionMapper;
import com.crater.auth.provider.ValidationExceptionMapper;
import com.crater.auth.resource.ClientResource;
import com.crater.auth.resource.ProjectResource;
import com.crater.auth.resource.TokenResource;
import com.crater.auth.resource.impl.ClientResourceImpl;
import com.crater.auth.resource.impl.ProjectResourceImpl;
import com.crater.auth.resource.impl.TokenResourceImpl;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * @author Navrattan Yadav
 *
 */
public class AuthApplication extends EmbeddableApplication<AuthServiceConfiguration> implements Managed {
	
	private ClassPathXmlApplicationContext classPathXmlApplicationContext;
	
	public static void main(String[] args) throws Exception {
		new AuthApplication().run(Lists.newArrayList("server", "auth-service.yaml").toArray( new String[2]));
	}
	@Override
	public void run(AuthServiceConfiguration configuration,
			Environment environment) throws Exception {
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring/application-config.xml");
		
		/**
		 * Add Health Check Service
		 */
		final AuthHealthCheck healthCheck = classPathXmlApplicationContext.getBean(AuthHealthCheck.class);
		environment.healthChecks().register("Auth Health Check", healthCheck);
		
		/**
		 * Register Filter
		 */
		final AuthorizationFilter authorizationFilter = classPathXmlApplicationContext
				.getBean(AuthorizationFilter.class);
		environment.jersey().register(authorizationFilter);
		
		/**
		 * Register Exception Mapper
		 */
		environment.jersey().register(CustomExceptionMapper.class);
		environment.jersey().register(ValidationExceptionMapper.class);
		
		/**
		 * Register Resources
		 */
		final ProjectResource projectResource = classPathXmlApplicationContext.getBean(ProjectResourceImpl.class);
		final ClientResource clientResource = classPathXmlApplicationContext.getBean(ClientResourceImpl.class);
		final TokenResource tokenResource = classPathXmlApplicationContext.getBean(TokenResourceImpl.class);
		environment.jersey().register(projectResource);
		environment.jersey().register(tokenResource);
		environment.jersey().register(clientResource);
		
		environment.getObjectMapper().enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
		environment.getObjectMapper().enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
		
		/**
		 * Other
		 */
		environment.getObjectMapper().registerModule(new JodaModule());
		
		/**
		 * Register Life cycle Manage Listener
		 */
		environment.lifecycle().manage(this);
	}

	public void start() throws Exception {
	}

	public void stop() throws Exception {
		classPathXmlApplicationContext.close();
	}
}
