/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.application;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;

/**
 * Extends DropWizard Application allowing it to start/stop embedded Application
 * @author Navrattan Yadav
 *
 */
public abstract class EmbeddableApplication<T extends Configuration> extends Application<T> {

	 private final EmbeddedServerCommand<T> embeddedServerCommand =
	            new EmbeddedServerCommand<T>(this);

	    public void startEmbeddedServer(String configFileName) throws Exception {
	        run(new String[] {"embedded-server", configFileName});
	    }

	    public void stopEmbeddedServer() throws Exception {
	        embeddedServerCommand.stop();
	    }

	    public boolean isEmbeddedServerRunning() {
	        return embeddedServerCommand.isRunning();
	    }

	    @Override
	    public void initialize(Bootstrap <T> bootstrap) {
	        bootstrap.addCommand(embeddedServerCommand);
	    }
}
