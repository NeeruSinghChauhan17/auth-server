/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service;


/**
 * @author Navrattan Yadav
 *
 */
public interface AuthorizationService {

	public abstract boolean authorized(String secretId,String token, boolean isCreator);
	public abstract boolean authorizedAdmin(String secretId,String token);
	public abstract void add(int secretId, String secret, int principle);
}
