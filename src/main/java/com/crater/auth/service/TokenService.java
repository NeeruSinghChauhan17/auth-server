/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service;

import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.AccessToken;

/**
 * @author Navrattan Yadav
 *
 */
public interface TokenService {

	public abstract AccessToken create(int projectId, int clientId, AccessToken token ) throws ForbiddenException;
	public abstract boolean authorize(int projectId, AccessToken token);
	public abstract void delete(int projectId, AccessToken token);
	
}
