/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service;

import java.util.List;

import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;

/**
 * @author Navrattan Yadav
 *
 */
public interface ProjectService {
	
	public abstract Project add(Project project) throws ConflitException;
	public abstract void delete(int projectId) throws ForbiddenException;
	public abstract Project get(int projectId) throws ForbiddenException;
	public abstract List<Project> list(int count);
	public abstract void updateTokenExpireTime(int projectId,
			TokenExpireTime tokenExpireTime)throws ForbiddenException;
	public abstract TokenExpireTime getTokenExpireTime(int projectId) throws ForbiddenException;
}
