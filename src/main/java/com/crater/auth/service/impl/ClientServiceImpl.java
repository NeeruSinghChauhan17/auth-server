/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service.impl;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.dao.ClientDao;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Client;
import com.crater.auth.model.TokenPermission;
import com.crater.auth.service.ClientService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class ClientServiceImpl implements ClientService {

	private final ClientDao clientDao;
	
	@Autowired
	public ClientServiceImpl(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

	@Override
	public Client create(int projectId, Client client) throws ConflitException {
		/**
		 * Generate a random secret for a client 
		 */
		final String secret = RandomStringUtils.random(10, true, true);
		final Client clientWithSecret = Client.createClientWithSecret(client, secret);
		return clientDao.create(projectId, clientWithSecret);
	}

	@Override
	public void delete(int clientId) throws ForbiddenException {
		clientDao.delete(clientId);
	}

	@Override
	public void updatePermission(int clientId, TokenPermission permission)
			throws ForbiddenException {
		clientDao.updatePermission(clientId, permission);
	}

	@Override
	public Client getClientById(int clientId) throws ForbiddenException {
		return clientDao.getClientById(clientId);
	}

	@Override
	public List<Client> getAllClientForProjectId(int projectId) {
		return clientDao.getAllClientForProjectId(projectId);
	}
}
