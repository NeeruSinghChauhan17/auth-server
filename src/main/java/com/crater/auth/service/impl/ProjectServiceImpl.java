/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.dao.ProjectDao;
import com.crater.auth.exception.ConflitException;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Project;
import com.crater.auth.model.TokenExpireTime;
import com.crater.auth.service.ProjectService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class ProjectServiceImpl implements ProjectService {

	private final ProjectDao projectDao;
	
	private final long MILI_SEC_IN_8_HOURS = 8 * 60 * 60 * 1000;
	
	
	@Autowired
	public ProjectServiceImpl(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	@Override
	public Project add(Project project) throws ConflitException {
		final Project p = Project.createWithTokenExpireTime(project, new TokenExpireTime(MILI_SEC_IN_8_HOURS, MILI_SEC_IN_8_HOURS));
		return projectDao.create(p);
	}

	@Override
	public void delete(int projectId) throws ForbiddenException {
		projectDao.delete(projectId);
	}

	@Override
	public void updateTokenExpireTime(int projectId,
			TokenExpireTime tokenExpireTime) throws ForbiddenException {
		projectDao.updateTokenExpireTime(projectId, tokenExpireTime);
	}

	@Override
	public TokenExpireTime getTokenExpireTime(int projectId) throws ForbiddenException {
		return projectDao.getTokenExpireTime(projectId);
	}

	@Override
	public Project get(int projectId) throws ForbiddenException {
		return projectDao.getProjectByProjectId(projectId);
	}

	
	@Override
	public List<Project> list(int count) {
		return projectDao.list(count * 20 , 20);
	}

}
