/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service.impl;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.perf4j.aop.Profiled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.common.TokenHelper;
import com.crater.auth.dao.ProjectDao;
import com.crater.auth.dao.TokenDao;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.AccessToken;
import com.crater.auth.model.TokenExpireTime;
import com.crater.auth.service.TokenService;
import com.crater.ua.parser.UserAgentParser;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class TokenServiceImpl  implements TokenService {

	private final TokenDao tokenDao;
	private final ProjectDao projectDao;
	
	private final TokenHelper tokenHelper;
	
	
	@Autowired
	public TokenServiceImpl(TokenDao tokenDao, ProjectDao projectDao, TokenHelper tokenHelper) {
		this.tokenDao = tokenDao;
		this.tokenHelper = tokenHelper;
		this.projectDao = projectDao;
	}

	@Profiled(tag="createToken.service")
	@Override
	public AccessToken create(int projectId, int clientId, AccessToken accessToken) 
			throws ForbiddenException {
		
		/**
		 * Get Token Expire time For Project
		 */
		final TokenExpireTime time = projectDao.getTokenExpireTime(projectId);
		
		final long tokenExpireTime = new UserAgentParser(accessToken.getUserAgent()).isMobile() ? 
				time.getMobileTokenExpireTime() + DateTime.now(DateTimeZone.UTC).getMillis() :
					time.getWebTokenExpireTime() + DateTime.now(DateTimeZone.UTC).getMillis();
		
		final String token = tokenHelper.generateToken( accessToken.getUserId(), tokenExpireTime );
		
		final AccessToken accessTokenWithToken = AccessToken.createAccessTokenWithToken(accessToken, token);
		
		tokenDao.save(projectId, accessTokenWithToken);
		return accessTokenWithToken;
	}

	@Profiled(tag="authorizeToken.service")
	@Override
	public boolean authorize(int projectId, AccessToken token) {
		if (tokenHelper.isValidToken(token.getToken(), token.getUserAgent())) {
			final String tkn = tokenDao.get(projectId, token.getUserId(), token.getUserAgent());
			return (tkn != null && tkn.equals(token.getToken()));
		}
		return false;
	}

	@Profiled(tag="deleteToken.service")
	@Override
	public void delete(int projectId, AccessToken token) {
		tokenDao.delete(projectId, token.getUserId(), token.getUserAgent());
	}

}
