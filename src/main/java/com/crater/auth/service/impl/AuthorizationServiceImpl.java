/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.service.impl;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.dao.ClientDao;
import com.crater.auth.exception.ForbiddenException;
import com.crater.auth.model.Client;
import com.crater.auth.model.TokenPermission;
import com.crater.auth.service.AuthorizationService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class AuthorizationServiceImpl implements AuthorizationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationServiceImpl.class);

	private final ClientDao clientDao;
	
	@Autowired
	public AuthorizationServiceImpl(ClientDao clientDao) {
		this.clientDao = clientDao;
	}
	
	@Profiled(tag="authorizeUser.service")
	@Override
	public boolean authorized(String secretId, String secret, boolean isCreator) {
		try {
			final Client client = clientDao.getClientById(Integer.valueOf(secretId));
			if(isCreator && client.getPermission() != TokenPermission.CREATE_AND_AUTHORIZE) {
				return false;
			}
			return client.getSecret().equals(secret);
		} catch (NumberFormatException |ForbiddenException e) {
			return false;
		}
	}

	@Override
	public boolean authorizedAdmin(String secretId, String secret) {
		LOGGER.info("Inside authorization service " + secretId + " token " + secret);
		return secretId.equals(secret);
	}

	@Override
	public void add(int secretId, String secret, int principle) {
		
	}

}
