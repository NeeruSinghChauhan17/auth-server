package com.crater.auth.common.aes;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AESCryptography {

	private final Logger _logger = LoggerFactory.getLogger(AESCryptography.class);
	
	private String encryptionKey;
	private SecretKeySpec key;
	/**
	 * Possible value are : 128,192,256
	 */
	private String keyLength;
	private String iv;
	
	public AESCryptography(String encryptionKey, String iv, String keyLength) {
		this.keyLength = keyLength;
		this.encryptionKey = encryptionKey;
		this.iv = iv;
	}
	
	@PostConstruct
	public void initIt() {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-" + keyLength);
			byte[] key = md.digest(encryptionKey.getBytes("UTF-8"));
			this.key = new SecretKeySpec(key, "AES");
		} catch (UnsupportedEncodingException e) {
			_logger.error("error while initilizing Cryptography", e);
		} catch (NoSuchAlgorithmException e) {
			_logger.error("error while initilizing Cryptography", e);
		}
	}
	
	public String encrypt(String text) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, this.key, new IvParameterSpec(iv.getBytes("UTF-8")));
			return Base64.encodeBytes(cipher.doFinal(text.getBytes()));
		} catch (Exception e) {
			_logger.error("error while encrypt ", e);
			throw new RuntimeException(e);
		}
	}
	
	public String decrypt(String text) {
		String decrypted = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			System.out.println(" key ="+this.key);
			cipher.init(Cipher.DECRYPT_MODE,this.key, new IvParameterSpec(iv.getBytes("UTF-8")));
			decrypted = new String(cipher.doFinal(Base64.decode(text)));
		} catch (Exception e) {
			_logger.error("error while decrypt ", e);
			throw new RuntimeException(e);
		}
		return decrypted;
	}
}
