/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.common;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.common.aes.AESCryptography;
import com.crater.ua.parser.UserAgentParser;


/**
 * @author Navrattan Yadav
 *
 */
@Service
public class TokenHelper {

	private final Logger LOGGER = LoggerFactory.getLogger(TokenHelper.class);
	
	
	public static final int EXPIRE_TIME_LENGHT = 13;
	public static final int MIN_TOKEN_LENGTH = EXPIRE_TIME_LENGHT + 1; //for column + 1;

	@Autowired
	AESCryptography aesCryptography;
	
	public String generateToken(String userId, long expireTime) {
		String encryptString = userId + "," + expireTime;
		return expireTime + ":" + aesCryptography.encrypt(encryptString);
	}

	public boolean isValidToken(String token, String userAgent) {
		if(token == null || token.isEmpty()) {
			return false;
		}
		try {

			/**
			 * Token Sample : 123343434:nfkskdflkanklsjmsd90kndas
			 */
			
			/**
			 * Validate index of : in token 
			 */
			int columnIndex = token.indexOf(':');
			if (columnIndex > 0 && token.length() > MIN_TOKEN_LENGTH) {
				/**
				 * Get Expire time from token
				 */
				long expireTime1 = Long.parseLong(token.substring(0,
						columnIndex));
				/**
				 * Get Encrypted String from token and Decrypt it.
				 * Decrypted String sample :  "userId,ExpireTime"
				 */
				String encryptedPart = token.substring(columnIndex + 1);
				String decryptedString = aesCryptography.decrypt(encryptedPart);
				
				/**
				 * Validate index of , in decrypted string
				 */
				int speratorIndex = decryptedString.indexOf(',');
				if (speratorIndex > 0) {
					/**
					 * Get expireTime from decrypted String and compare with other one 
					 * Also check expire from current time.
					 * Expire time only check for web token.
					 * a mobile token never expire even it has expire time.
					 */
					long expireTime2 = Long.parseLong(decryptedString
							.substring(speratorIndex + 1));
					if (expireTime1 == expireTime2
							&& (new UserAgentParser(userAgent).isMobile() || 
									expireTime1 > DateTime.now(DateTimeZone.UTC).getMillis())) {
						LOGGER.info("token valid");
						return true;
					}
					 
				}
			}
		} catch (Exception e) {
			LOGGER.error("error while check is valid token", e);
		}
		return false;
	}
	
}
