/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.provider;

import java.util.ArrayList;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Navrattan Yadav
 *
 */

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

	/* (non-Javadoc)
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	
	@Override
	public Response toResponse(ConstraintViolationException e) {
		 ArrayList<String> errors = new ArrayList<String>();
	        for (ConstraintViolation<?> methodConstraintViolation : e.getConstraintViolations()) {
	            errors.add( methodConstraintViolation.getMessage());
	        }
	        
	        ValidationError validationError = new ValidationError(errors);
		return Response.status(Status.BAD_REQUEST)
				.entity(validationError)
				.type(MediaType.APPLICATION_JSON).build();
	}

 
	public static class ValidationError {
		private ArrayList<String> errors;
		
		

		public ValidationError(ArrayList<String> errors) {
			this.errors = errors;
		}
		
		public ValidationError() {
		}

		public ArrayList<String> getErrors() {
			return errors;
		}

		public void setErrors(ArrayList<String> errors) {
			this.errors = errors;
		}
		
	}
}