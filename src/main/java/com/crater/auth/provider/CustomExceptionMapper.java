/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.provider;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.crater.auth.exception.CustomException;

/**
 * @author Navrattan Yadav
 *
 */
@Provider
public class CustomExceptionMapper implements ExceptionMapper<CustomException> {

	public CustomExceptionMapper() {
	}

	@Override
	public Response toResponse(CustomException e) {
		return Response.status(e.getStatus().value())
				.entity(e.getMessage())
				.type(MediaType.APPLICATION_JSON).build();
	}

}
