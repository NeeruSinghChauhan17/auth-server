/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.filter;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Navrattan Yadav
 *
 */
@Aspect
public class Logging {

	private static final Logger LOGGER = LoggerFactory.getLogger(Logging.class);
	
//	@Pointcut("execution(* com.crater.auth.resource.ClientResource.*(..))")
	@Pointcut("@annotation(com.crater.auth.annotation.Log)")
	private void register() {
		
	}
	@Before("register()")
	public void beforClientReg() {
		LOGGER.info("Registering Client");
	}
	
	@AfterReturning(pointcut="register()", returning="ob")
	public void afterClientRegSuccess(Object ob) {
		LOGGER.info("Client Registered client = {}", ob.toString());
	}
	
	@AfterThrowing(pointcut="register()", throwing="e")
	public void afterThrow(Exception e) {
		LOGGER.error("Client Register Error = {} ", e.getMessage());
	}
	
}
