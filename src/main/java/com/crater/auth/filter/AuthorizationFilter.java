/*
 * Copyright (C) 2014 Craterzone Pvt. Ltd. 
 */
package com.crater.auth.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.auth.annotation.AdminRole;
import com.crater.auth.annotation.Creator;
import com.crater.auth.annotation.UserRole;
import com.crater.auth.service.AuthorizationService;

/**
 * @author Navrattan Yadav
 *
 */
@Service
public class AuthorizationFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilter.class);
	
	private final AuthorizationService authorizationService;
	
	@Context
	private ResourceInfo resourceInfo;

	@Autowired
	public AuthorizationFilter(AuthorizationService authorizationService) {
		this.authorizationService = authorizationService;
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {
		if(resourceInfo.getResourceMethod().isAnnotationPresent(UserRole.class)) {
			String secretId = requestContext.getHeaderString("secretId");
			String secret = requestContext.getHeaderString("secret");
			boolean isCreator = resourceInfo.getResourceMethod().isAnnotationPresent(Creator.class);
			if(isEmpty(secret) || isEmpty(secretId)) {
				LOGGER.info("Aborting unauthorized request for secretId={} and token={}", secretId, secret);
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity("secret or secretId not present").build());
			} else if(!authorizationService.authorized(secretId, secret, isCreator)){
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity("secret not valid").build());
			}
		} else if (resourceInfo.getResourceMethod().isAnnotationPresent(AdminRole.class)) {
			String secretId = requestContext.getHeaderString("secretId");
			String secret = requestContext.getHeaderString("secret");
			if(isEmpty(secret) || isEmpty(secretId)) {
				LOGGER.info("Aborting unauthorized request for secretId={} and token={}", secretId, secret);
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity("secret or secretId not present").build());
			} else if(!authorizationService.authorizedAdmin(secretId, secret)){
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity("secret not valid").build());
			}
		} 
	}
	
	
	/**
	 * This method check if a <Code>String<Code> is null or empty
	 * @param text
	 * @return true if String is null or has length 0 otherwise false
	 */
	private boolean isEmpty(String text) {
		if(text == null || text.isEmpty()) {
			return true;
		}
		return false;
	}
	
}
